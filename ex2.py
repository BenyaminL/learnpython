# A comment, this so you could read your program later
# Anything after '#' is ignored by python
print "I could have code like this!" # and the command after is ignored
# You could use comment or disable a piece of code. 
# print "This won't run"

print "This will run."
