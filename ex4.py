#Learn Excercise 4
# Variable untuk jumlah mobil yang ada
cars = 100
# Variable untuk jumlah driver yang tersedia
driver = 30
# Jumlah kursi setiap mobil yang dimiliki sama car pool
# space_in_cars = 4.0 #Ini bagian yang pakai floating point
# Use integer not float variable 
space_in_cars = 4 # Tidak pakai Floating Point
# First Question of the Riddle on LPTHW 
# 1. It's cause the carpool_capacity output no float atau mudahnya sisanya ga ada, jadi ga pecahan hasil outputnya
# 2. Jawaban untuk yang soal ke dua : https://www.google.co.id/url?sa=t&rct=j&q=&esrc=s&source=web&cd=3&cad=rja&uact=8&ved=0ahUKEwig9rG9_L_VAhXJMY8KHUG-CNkQFggxMAI&url=https%3A%2F%2Fid.wikipedia.org%2Fwiki%2FFloating-point&usg=AFQjCNHFMhC9MxwwcN_QM7QvFeBur1yO9g
# 3. Terjawan untuk setiap variable yang diberi komentar
# Jumlah Penumpang yang ada
# 4. Dalam menuliskan == dan = ada perbedaan, kalau = untuk mengisi sebuah variable, sedangkan untuk == untuk perbandingan, sebuah operator perbandingan, sejenis itu. Jadi cukup tahu aja :v
passangers = 90
# Mobil yang tidak punya sopir, jadi ga bisa jalan ;)
cars_not_driven = cars - driver
# Mobil yang bisa jalan karena ada sopir
cars_driven = driver
# Jumlah tempat dari setiap mobil yang ada
carpool_capacity = space_in_cars * cars_driven
# Jumlah Penumpang tiap mobil yang akan keluar dari pool
average_passangers_per_car = passangers / cars_driven

print "There're car ", cars, " available"
print "But There's only ", driver, " available drivers"
print "There will be ", cars_not_driven, " cars not driven today"
print "So we can transfer ", carpool_capacity, " people today"
print "We have about ", passangers, " passengers today."
print "So we need to put about ", average_passangers_per_car, " people per car so the passangers can be transfered to the place they would to go"
